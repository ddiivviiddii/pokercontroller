#include <RCSwitch.h>

// key codes for keyboard shortcuts
#define FoldCode         48
#define FoldToAnyCode    49
#define CheckCode        50
#define CheckAllCode     51
#define CallCode         52
#define BetRaiseCode     53
#define AllInCode        54
#define Bet25Code        55
#define Bet50Code        56
#define Bet100Code       57

// �������� pin'��
#define FoldPin         2
#define FoldToAnyPin    3
#define CheckPin        4
#define CheckAllPin     5
#define CallPin         6
#define BetRaisePin     7
#define AllInPin        8
#define Bet50Pin        14  // A0
#define Bet100Pin       15  // A1
#define Bet25Pin        16  // A2
#define PowerLED        17  // A3

#define	RFPin			10		// pin where RF315 is connected

RCSwitch mySwitch = RCSwitch();

void  setup(void)
{
	pinMode(FoldPin, INPUT_PULLUP);
	pinMode(FoldToAnyPin, INPUT_PULLUP);
	pinMode(CheckPin, INPUT_PULLUP);
	pinMode(CheckAllPin, INPUT_PULLUP);
	pinMode(CallPin, INPUT_PULLUP);
	pinMode(BetRaisePin, INPUT_PULLUP);
	pinMode(AllInPin, INPUT_PULLUP);
	pinMode(Bet50Pin, INPUT_PULLUP);
	pinMode(Bet100Pin, INPUT_PULLUP);
	pinMode(Bet25Pin, INPUT_PULLUP);
	pinMode(PowerLED, OUTPUT);

	randomSeed(analogRead(4));		//trying to get real random	

	mySwitch.enableTransmit(RFPin);
	mySwitch.setProtocol(2);		// protocol "2" works better for me but in documentation suggested number one
}
void loop()
{

	if (digitalRead(FoldPin) == HIGH) SendCode(FoldCode);
	if (digitalRead(FoldToAnyPin) == HIGH) SendCode(FoldToAnyCode);
	if (digitalRead(CheckPin) == HIGH) SendCode(CheckCode);
	if (digitalRead(CheckAllPin) == HIGH) SendCode(CheckAllCode);
	if (digitalRead(CallPin) == HIGH) SendCode(CallCode);
	if (digitalRead(BetRaisePin) == HIGH) SendCode(BetRaiseCode);
	if (digitalRead(AllInPin) == HIGH) SendCode(AllInCode);
	if (digitalRead(Bet25Pin) == HIGH) SendCode(Bet25Code);
	if (digitalRead(Bet50Pin) == HIGH) SendCode(Bet50Code);
	if (digitalRead(Bet100Pin) == HIGH) SendCode(Bet100Code);

}


void SendCode(int code)
{
	int salt;
	salt = 100 * random(10, 99);	// generating "salt" to identify unique key press
	code = code + salt;				// adding "salt" to code for send
	mySwitch.send(code, 24);		// sending code with "salt"
	delay(100);						// small delay to exclude double key's press
}